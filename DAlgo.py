import os
import sys
import argparse
from copy import deepcopy

from gates.base_gate import Gate
from builder import build_network


class DAlgo(object):

    def __init__(self, netlist_path):
        self.netlist_path = netlist_path

    def initialize_network(self):
        g,l,i,o = build_network(self.netlist_path)
        self.gates = g
        self.lines = dict.fromkeys(l,'X')
        self.input_lines = i
        self.output_lines = o

    def reset_network(self):
        for gate in self.gates:
            for name in gate.inputs:
                gate.inputs[name] = 'X'
            for name in gate.outputs:
                gate.outputs[name] = 'X'
        for name in self.lines:
            self.lines[name] = 'X'

    def update_lines(self, update_values):
        if isinstance(update_values, Gate):
            for name in update_values.inputs:
                self.lines[name] = update_values.inputs[name]
            for name in update_values.outputs:
                self.lines[name] = update_values.outputs[name]
        else:
            for name in update_values:
                self.lines[name] = update_values[name]

    def construct_pdf(self, line, fault):
        if line in self.input_lines:
            self.update_lines({line:'D'})
            for gate in self.gates:
                if line in gate.inputs:
                    self.d_front.append(gate)
        for gate in self.gates:
            if line in gate.outputs:
                inputs, outputs = gate.set_fault(fault)
                self.update_lines(inputs)
                self.update_lines(outputs)
                self.d_front.extend(gate.parents)
                for name in gate.inputs:
                    for child in gate.children:
                        if name in child.outputs:
                            child.outputs[name] = gate.inputs[name]
                            self.j_front.insert(0, child)

    def save_history(self):
        self.sd_front = deepcopy(self.d_front)
        self.sj_front = deepcopy(self.j_front)
        self.slines = deepcopy(self.lines)
        self.sgates = self.gates

    def backtrack(self):
        self.d_front = self.sd_front
        self.j_front = self.sj_front
        self.lines = self.slines
        self.gate = self.sgates


    def consistent(self, scheme, gate):
        consistent_assignment = True
        for value, name in zip(scheme, gate.inputs):
            if self.lines[name] != value and \
               self.lines[name] != 'X':
                consistent_assignment = False
        return consistent_assignment

    def update_all_fronts(self, gate):
        for parent in gate.parents:
            self.d_front.insert(0, parent)
            for out in gate.outputs:
                if out in parent.inputs:
                    self.faults.insert(0, (out, gate.logical_output()))
        self.update_j_frontier(gate)

    def update_j_frontier(self, gate):
        for child in gate.children:
            if not child.consistent:
                self.j_front.insert(0, child)

    def finished_propagation(self):
        for line in self.output_lines:
            if self.lines[line] in ['D', 'D-']:
                return True
        return False

    def create_testvectors(self):
        self.initialize_network()
        test_vectors = []
        for line in self.lines:
            for fault in ["SA1", "SA0"]:
                vector = [line, fault]
                vector.append(self.d_algorithm(line, fault))
                test_vectors.append(vector)
                self.reset_network()
        self.write_output(test_vectors)

    def d_algorithm(self, line, fault):
        print('Line: ',line,'; Fault: ',fault)
        self.d_front, self.j_front = [], []
        self.faults = [(line, 'D')]
        self.construct_pdf(line, fault)
        while not self.finished_propagation():
            self.save_history()
            gate = self.d_front.pop()
            fault = self.faults.pop()
            gate.set_propagation_scheme(fault[1])
            while gate.untried_propagation_scheme:
                scheme = gate.untried_propagation_scheme.pop()
                if self.consistent(scheme, gate):
                    gate.update(scheme, propagation=True)
                    self.update_all_fronts(gate)
                    self.update_lines(gate)
                    break
            if not gate.consistent:
                if len(self.d_front) == 0:
                    return False
                else:
                    self.backtrack()
        while self.j_front:
            self.save_history()
            gate = self.j_front.pop()
            gate.set_justification_scheme()
            while gate.untried_justification_scheme:
                scheme = gate.untried_justification_scheme.pop()
                if self.consistent(scheme, gate):
                    gate.update(scheme, propagation=False)
                    self.update_j_frontier(gate)
                    self.update_lines(gate)
                    break
            if not gate.consistent:
                self.backtrack()
        vector = {}
        for key in self.input_lines:
            vector[key] = self.lines[key]
        for key in self.output_lines:
            vector[key] = self.lines[key]
        return vector

    def write_output(self, test_vectors):
        keys = []
        keys.extend(self.input_lines)
        keys.extend(self.output_lines)
        path, name = os.path.split(self.netlist_path)
        new_name = name.split('.')[0]+'.test'
        header = 'Line, Fault,'+','.join(keys)
        with open(os.path.join(path,new_name), 'w') as f:
            f.write(header+'\n')
            for test in test_vectors:
                line = ','.join(test[:2])
                for key in keys:
                    line += ','+test[2][key]
                f.write(line+'\n')
        print("Finished...")


if __name__ == '__main__':
    # Test = DAlgo('netlists/full_example.cir')
    ##
    # Test.initialize_network()
    # Test.d_algorithm('1','SA0')
    
    # Test.create_testvectors()
    # sys.exit()

    parser = argparse.ArgumentParser('Implementation of the D-Algorithm')
    parser.add_argument("-p", "--path", help="Path to netlist.")

    args = parser.parse_args()
    if os.path.exists(args.path):
        print("Starting D-Algorithm:")
        Worker = DAlgo(args.path)
        Worker.create_testvectors()
    else:
        print('Invalid input-path: ',args.path)
